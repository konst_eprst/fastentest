/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.Tokens;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.noip.konst.fastencrud.Account.Account;

/**
 *
 * @author Kostya
 */
@Entity
@Table(name = "Tokens")
public class Token implements Serializable{
    private static final long serialVersionUID = 1L;
    
    @Id
    @NotNull
    @Column(name = "Token", updatable = false)
    private final String token;
    
    
    @Column(name = "DateExpired")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDateTime;

    @JoinColumn(name = "UserId", referencedColumnName = "Id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Account user;
    
    protected Token() {
        this.token = generateToken();
        this.expirationDateTime = Calendar.getInstance().getTime();
    }
    
    /**
     * Конструктор для внутренних целей. 
     * @param token 
     */
    Token(String token){
        this.token = token;
        this.expirationDateTime = Calendar.getInstance().getTime();
    }
    
    /**
     * Сгенерировать новый токен
     *
     * @param date Дата истечения срока действия токена
     */
    Token(Date date) {
        this.token = generateToken();
        this.expirationDateTime = date;
    }

    private String generateToken() {
        return UUID.randomUUID().toString();
    }

    public String getToken() {
        return this.token;
    }

    public Date getExpirationDateTime() {
        return this.expirationDateTime;
    }
    
    public void setExpirationTime(Date expTime){
        this.expirationDateTime = expTime;
    }

    @Override
    public int hashCode() {
        return this.token.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Token other = (Token) obj;
        if (!Objects.equals(this.token, other.token)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.token;
    }

    /**
     * @return the user
     */
    public Account getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(Account user) {
        this.user = user;
    }
    
    
    
}

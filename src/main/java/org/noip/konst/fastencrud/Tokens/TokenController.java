/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.Tokens;

import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;


/**
 *
 * @author Kostya
 */

@Stateless
public class TokenController {

    @PersistenceContext(unitName = "FastenPU")
    private EntityManager em;

    @Transactional
    public void addTokenToHistory(Token token){
        em.persist(token);
    }
    
    @Transactional
    public void addTokenToHistory(Set<Token> tokens){
        tokens.forEach(t -> em.persist(t));
    }
}

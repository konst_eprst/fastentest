/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.Tokens;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import org.noip.konst.fastencrud.Account.Account;

/**
 * Класс для работы с токенами
 *
 * @author Kostya
 */
@ApplicationScoped
public class TokensCache {

    /**
     * Коллекция действующих токенов
     */
    //TODO: Заменить Set<Token> на Map<Token, User>. Так как у Set нельзя быстро найти и получить элемент.
    private final Map<Token, Account> tokens = new ConcurrentHashMap<>();

    /**
     * Поток, отвечающий за удаление токенов с истекшим сроком действия
     */
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    /**
     * Время жизни токена в минутах
     */
    private Integer timeToLive = 10;

    @Inject
    private TokenController tokenController;

    private final Object lockObject = new Object();
    
    /*
     * Запуск потока, ответственного за очистку коллекции токенов
     */
    @PostConstruct
    private void startScheduler() {
        System.out.println("Запуск таймера для удаления старых токенов");
        final Runnable checker = () -> {
            deleteExpiredTokens();
        };

        final ScheduledFuture<?> checkerHandle
                = scheduler.scheduleAtFixedRate(checker, 0, 1, TimeUnit.MINUTES);
    }

    /*
     * Остановка потока, ответственного за очистку коллекции токенов
     */
    @PreDestroy
    private void stopScheduler() {
        scheduler.shutdown();
    }

    /**
     * Процедура проверки и удаления токенов, чей срок действия истек.
     */
    private void deleteExpiredTokens() {
        Set<Token> toDelete = tokens.keySet()
                .parallelStream()
                .filter(t -> Calendar.getInstance().getTime().after(t.getExpirationDateTime()))
                .collect(Collectors.toSet());

        if(toDelete.size() > 0){
            removeToken(toDelete);
        }
    }

    /**
     * Сгенерировать и привязать новый токен для пользователя
     *
     * @param user Пользователь, для которого генерируется новый токен
     */
    public void generateToken(Account user) {
        if (user == null) {
            return;
        }

        if (user.getToken() != null) {
            removeToken(user.getToken());
        }

        removeUserTokens(user);

        Date date = Calendar.getInstance().getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, timeToLive);
        Token newToken = new Token(cal.getTime());

        newToken.setUser(user);
        user.setToken(newToken.getToken());

        tokens.put(newToken, user);
    }

    private void removeUserTokens(Account user) {
        Set<Token> tokensToRemove = tokens.entrySet()
                .parallelStream()
                .filter(e -> e.getValue().equals(user))
                .map(e -> e.getKey())
                .collect(Collectors.toSet());

        removeToken(tokensToRemove);
    }

    /**
     * Проверить токен на активность
     *
     * @param token
     * @return true - токен активен. false - токен закончил своё действие
     */
    public boolean checkToken(String token) {
        return tokens.containsKey(new Token(token));
    }
    
    /**
     * Проверить токен на активность
     *
     * @param token
     * @return true - токен активен. false - токен закончил своё действие
     */
    public boolean checkToken(Token token) {
        return tokens.containsKey(token);
    }

    public Account getUser(String Token) {
        Token t = new Token(Token);

        return tokens.get(t);
    }

    /**
     * Установить время жизни токена
     *
     * @param timeToLive Время жизни в минутах
     */
    public void setTokenExpirationPeriod(int timeToLive) {
        this.timeToLive = timeToLive;
    }

    /**
     * Закончить действие токена
     *
     * @param token Токен, действие которого должно быть закончено
     */
    @Transactional
    public boolean removeToken(String token) {
        Token t = new Token(token);
        t.setUser(getUser(token));

        synchronized(lockObject)  {
            if (checkToken(token)) {
                tokens.remove(t);
                sendToHistory(t);
            }
        }

        return true;
    }
    
    /**
     * Закончить действие токена
     *
     * @param token Токен, действие которого должно быть закончено
     */
    @Transactional
    public boolean removeToken(Token token) {
        if(token.getUser() != null){
            token.setUser(getUser(token.getToken()));
        }
        

        synchronized(lockObject)  {
            if (checkToken(token)) {
                tokens.remove(token);
                sendToHistory(token);
            }
        }

        return true;
    }

    /**
     * Закончить действие токенов
     *
     * @param tokensToRemove Токены, действие которых должно быть закончено
     */
    @Transactional
    public boolean removeToken(Set<Token> tokensToRemove) {
        
        tokensToRemove.forEach(t -> removeToken(t.getToken()));
        
        return true;
    }

    /**
     * Добавить токен в историю
     *
     * @param token
     *
     * Метод не удаляет токены из коллекции активных токенов
     */
    private void sendToHistory(Token token) {
        System.out.println("Удаляется токен: " + token.toString());

        token.setExpirationTime(Calendar.getInstance().getTime());
        tokenController.addTokenToHistory(token);
    }

    /**
     * Добавить токены в историю
     *
     * @param tokensToRemove
     *
     * Метод не удаляет токены из коллекции активных токенов
     */
    private void sendToHistory(Set<Token> tokensToRemove) {
        System.out.println("Удаляются токены: " + tokensToRemove.toString());

        tokensToRemove.forEach(t -> t.setExpirationTime(Calendar.getInstance().getTime()));
        tokenController.addTokenToHistory(tokensToRemove);
    }
}

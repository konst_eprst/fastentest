/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.decoders;

import javax.inject.Inject;
import javax.json.stream.JsonParsingException;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

import org.noip.konst.fastencrud.Messages.AbstractIncomingMessage;
import org.noip.konst.fastencrud.Messages.MessageFactory;

/**
 *
 * @author Kostya
 */
public class MessageDecoder implements Decoder.Text<AbstractIncomingMessage> {

    @Inject
    private MessageFactory mf;
    
    @Override
    public AbstractIncomingMessage decode(String arg0) throws DecodeException {
        
        AbstractIncomingMessage retVal;
        
        try{
            retVal = mf.parseIncomingMessage(arg0);
        } catch (JsonParsingException jpe){
            retVal = mf.generateIncomingErrorMessage("Ошибка парсинга JSON: " + jpe.getMessage(), "");
        } catch (Exception ex) {
            retVal = mf.generateIncomingErrorMessage("Ошибка парсинга JSON: " + ex.getMessage(), "");
        }
        
        /*
        try{
            retVal = MessageFactory.parseIncomingMessage(arg0);
        } catch (JsonParsingException jpe){
            retVal = MessageFactory.generateIncomingErrorMessage("Ошибка парсинга JSON: " + jpe.getMessage(), "");
        } catch (Exception ex) {
            retVal = MessageFactory.generateIncomingErrorMessage("Ошибка парсинга JSON: " + ex.getMessage(), "");
        }
          */  
                
        
        return retVal;
    }

    @Override
    public boolean willDecode(String arg0) {
        return true;
//        boolean retVal = validate(arg0);
//
//        return retVal;
    }

    @Override
    public void init(EndpointConfig config) {
        System.out.println("Декодер инициализирован");
    }

    @Override
    public void destroy() {
        System.out.println("Декодер уничтожен");
    }

    /**
     * Простейшая проверка на наличие нужных параметров
     *
     * @param jsonString Строка, получаемая от клиента
     * @return true - строка имеет поля data, type, sequence_id, api_token. В
     * противном случае - false
     *
     * Не проверяется соответствие формату JSON. Проверяется только наличие в
     * строке ключевых слов
     */
    private boolean validate(String jsonString) {
        final String pattern = "\\W*type\\W*:\\W*\\w*\\W*,\\W*sequence_id\\W*:\\W*[0-9\\w\\-]*\\W*,\\W*api_token\\W*:\\W*[0-9\\w\\-]*\\W*,\\W*data\\W*";

        return jsonString.matches(pattern);
    }
}

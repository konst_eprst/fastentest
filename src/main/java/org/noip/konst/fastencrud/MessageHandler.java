/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.annotation.PostConstruct;
import org.noip.konst.fastencrud.Messages.AbstractIncomingMessage;
import org.noip.konst.fastencrud.Messages.AbstractMessage;
import org.noip.konst.fastencrud.Messages.MessageFactory;
import org.noip.konst.fastencrud.Tokens.TokensCache;

/**
 *
 * @author Kostya
 */

@ApplicationScoped
public class MessageHandler {
    
    @Inject
    private TokensCache tokensCache;
    
    @Inject 
    private MessageFactory messageFactory;
    
    
    @PostConstruct
    private void init(){
        System.out.println("MessageHandler инициализирован");
    }
    
    public AbstractMessage proccesIncomingMessage(AbstractIncomingMessage message){
       
        return message.generateAnswer();

    }
}

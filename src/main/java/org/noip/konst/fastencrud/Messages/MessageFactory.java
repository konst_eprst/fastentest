/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.Messages;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParsingException;
import org.noip.konst.fastencrud.Tokens.TokensCache;
import org.noip.konst.fastencrud.Account.AccountController;
import org.noip.konst.fastencrud.Account.UserProfileController;

/**
 *
 * @author Kostya
 */
@ApplicationScoped
public class MessageFactory {

    @Inject private AccountController accountController;
    
    @Inject private UserProfileController userProfileController;
    
    @Inject private TokensCache tokensCache;
    
    public AbstractIncomingMessage parseIncomingMessage(String json) throws JsonParsingException {
        
        try (JsonReader jr = Json.createReader(new StringReader(json))) {

            JsonObject jo = jr.readObject();
            MessageType type = MessageType.valueOf(jo.getString(AbstractIncomingMessage.FIELD_TYPE));
            
            if(!type.equals(MessageType.CUSTOMER_LOGIN) 
                    && !checkAuthorization(jo.getString(AbstractIncomingMessage.FIELD_TOKEN))){
                return generateIncomingErrorMessage("Токен недействителен", jo.getString(AbstractIncomingMessage.FIELD_SEQUENCE_ID));
            }

            //TODO: Доделать создание классов
            switch (type) {
                case CUSTOMER_LOGIN:
                    return new LoginIncomingMessage(json, accountController, tokensCache);

                case CUSTOMER_DELETE:
                    break;

                case CUSTOMER_PROFILE:
                    return new ProfileIncomingMessage(json, accountController, tokensCache);
                    
                case CUSTOMER_PROFILE_UPDATE:
                    return new ProfileUpdateIncomingMessage(json, userProfileController, tokensCache);
                    
                case CUSTOMER_LOGOUT:
                    return new LogoutIncomingMessage(json, tokensCache);
            }

        }

        return null;
    }
    
    private boolean checkAuthorization(String token){
        return tokensCache.checkToken(token);
    }
    
    public AbstractIncomingMessage generateIncomingErrorMessage(String errorText, String sequenceId){
        Map<String, Object> data = new HashMap<>();
        data.put(AbstractIncomingMessage.FIELD_DATA_ERROR_TEXT, errorText);
        
        return new AbstractIncomingMessage(MessageType.CUSTOMER_ERROR, sequenceId, "", data) {
            @Override
            boolean doAction() {
                return true;
            }
            
            @Override
            public AbstractMessage generateAnswer() {
                return new AbstractMessage(MessageType.CUSTOMER_ERROR, sequenceId, data);
            }
        };
    }
    
}

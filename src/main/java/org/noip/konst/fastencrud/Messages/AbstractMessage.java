/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.Messages;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.stream.JsonParsingException;

/**
 *
 * @author Kostya
 */
public class AbstractMessage{
    
    public final static String FIELD_TYPE = "type";
    public final static String FIELD_SEQUENCE_ID = "sequence_id";
    public final static String FIELD_DATA = "data";
    public final static String FIELD_DATA_ERROR_TEXT = "error_text";
    
    private final MessageType type;
    private final String sequenceId;
    private final Map<String, Object> data;
    
    AbstractMessage(String json) throws JsonException, JsonParsingException{
        try(JsonReader jr = Json.createReader(new StringReader(json))){
            JsonObject obj = jr.readObject();
            type = MessageType.valueOf(obj.getString(AbstractIncomingMessage.FIELD_TYPE));
            sequenceId = obj.getString(AbstractIncomingMessage.FIELD_SEQUENCE_ID);
            
            data = new HashMap<>();
            JsonObject ja = obj.getJsonObject(FIELD_DATA);
            ja.entrySet().forEach((en) -> {
                data.put(en.getKey(), ja.getString(en.getKey()));
            });
        }
    }
    
    AbstractMessage(MessageType messageType, String sequenceId, Map<String, Object> data){
        this.type = messageType;
        this.sequenceId = sequenceId;
        this.data = data;
    }

    /**
     * @return the type
     */
    public MessageType getType() {
        return type;
    }

    /**
     * @return the sequenceId
     */
    public String getSequenceId() {
        return sequenceId;
    }

    /**
     * @return the data
     */
    public Map<String, Object> getData() {
        return data;
    }
    
    public JsonObject toJson(){
        JsonObjectBuilder builder = Json.createObjectBuilder();
        
        builder.add(FIELD_TYPE, getType().toString());
        builder.add(FIELD_SEQUENCE_ID, getSequenceId());
        
        JsonObjectBuilder dataBuilder = Json.createObjectBuilder();
        data.entrySet().forEach((entry) -> {
            dataBuilder.add(entry.getKey(), entry.getValue().toString());
        });
        builder.add(FIELD_DATA, dataBuilder.build());
        
        return builder.build();
    };

}

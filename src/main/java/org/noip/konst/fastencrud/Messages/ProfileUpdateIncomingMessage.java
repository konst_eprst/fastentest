/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.Messages;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.noip.konst.fastencrud.Account.Account;
import org.noip.konst.fastencrud.Account.UserProfile;
import org.noip.konst.fastencrud.Account.UserProfileController;
import org.noip.konst.fastencrud.Tokens.TokensCache;

/**
 *
 * @author Kostya
 */
public class ProfileUpdateIncomingMessage extends AbstractIncomingMessage{

    private Account user;
    private final UserProfileController userProfileController;
    private final TokensCache tokensCache;
    
    private final static String FIELD_PROFILE_UPDATE = "update";
    
    ProfileUpdateIncomingMessage(String jsonString, UserProfileController upc, TokensCache tc){
        super(jsonString);
        this.userProfileController = upc;
        this.tokensCache = tc;
    }
    
    
    @Override
    boolean doAction() {
        user = tokensCache.getUser(this.getToken());
        
        if(user != null){
            try{
                localProfileUpdate(getData());
                this.userProfileController.updateProfile(user.getProfile());
            } catch(NullPointerException npe){
                System.out.println("У пользователя нет профиля");
                return false;
            }
        } else {
            return false;
        }
        
        return true;
    }

    @Override
    public AbstractMessage generateAnswer() {
        Map<String, Object> data = new HashMap<>();
        MessageType type;
        
        if(!tokensCache.checkToken(getToken())){
            data.put(AbstractMessage.FIELD_DATA_ERROR_TEXT, "Токен не активен.");
            return new AbstractMessage(MessageType.CUSTOMER_ERROR, this.getSequenceId(), data);
        }
        
        if(doAction()){
            data.put(FIELD_PROFILE_UPDATE, "Профиль успешно обновлен");
            type = MessageType.CUSTOMER_SUCCESS;
        } else {
            data.put(AbstractMessage.FIELD_DATA_ERROR_TEXT, "Не удалось обновить профиль");
            type = MessageType.CUSTOMER_ERROR;
        }
        
        return new AbstractMessage(type, this.getSequenceId(), data);
    }

    private void localProfileUpdate(Map<String, Object> newData) throws NullPointerException{
        UserProfile profile = user.getProfile();
        
        for(Entry<String, Object> entry : newData.entrySet()){
            switch(entry.getKey()){
                case "lastName":
                    profile.setLastName(entry.getValue().toString());
                    break;
                    
                case "firstName":
                    profile.setFirstName(entry.getValue().toString());
                    break;
                    
                case "photo":
                    profile.setPhoto(entry.getValue().toString().getBytes());
                    break;
            }
        }
    }
    
}



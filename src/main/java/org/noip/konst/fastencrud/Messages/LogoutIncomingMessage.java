/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.Messages;

import java.util.HashMap;
import java.util.Map;
import org.noip.konst.fastencrud.Tokens.TokensCache;

/**
 *
 * @author Kostya
 */
public class LogoutIncomingMessage extends AbstractIncomingMessage{

    private static final String FIELD_LOGOUT = "logout";
    
    private final TokensCache tokensCache;

    LogoutIncomingMessage(String jsonString, TokensCache tc){
        super(jsonString);
        this.tokensCache = tc;
    }
    
    @Override
    boolean doAction() {
        return tokensCache.removeToken(this.getToken());
    }

    @Override
    public AbstractMessage generateAnswer() {
        
        Map<String, Object> data = new HashMap<>();
        MessageType type;
        
        if(doAction()){
            data.put(FIELD_LOGOUT, "Успешный выход из системы");
            type = MessageType.CUSTOMER_SUCCESS;
        } else {
            data.put(AbstractMessage.FIELD_DATA_ERROR_TEXT, "Ошибка при выходе из системы");
            type = MessageType.CUSTOMER_ERROR;
        }
        
        return new AbstractMessage(type, this.getSequenceId(), data);
    }
    
}

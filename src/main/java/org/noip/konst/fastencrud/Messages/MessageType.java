/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.Messages;

/**
 *
 * @author Kostya
 */
public enum MessageType {
    CUSTOMER_LOGIN,
    CUSTOMER_PROFILE,
    CUSTOMER_DELETE,
    CUSTOMER_ERROR,
    CUSTOMER_SUCCESS,
    CUSTOMER_PROFILE_UPDATE,
    CUSTOMER_LOGOUT
}

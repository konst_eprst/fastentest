/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.Messages;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.NoResultException;
import org.noip.konst.fastencrud.Tokens.TokensCache;
import org.noip.konst.fastencrud.Account.Account;
import org.noip.konst.fastencrud.Account.AccountController;

/**
 *
 * @author Kostya
 */
public class LoginIncomingMessage extends AbstractIncomingMessage{
    
    private Account user;
    private final AccountController uc;
    private final TokensCache tokensCache;

    LoginIncomingMessage(String jsonString, AccountController uc, TokensCache tc){
        super(jsonString);
        this.uc = uc;
        this.tokensCache = tc;
    }

    @Override
    boolean doAction() {
        boolean retVal;
        
        try{
            user = uc.authenticate(getData().get(Account.FIELD_LOGIN).toString(), getData().get(Account.FIELD_PASSWORD).toString());
            retVal = true;
        } catch (NoResultException ex){
            retVal = false;
        }
        
        
        return retVal;
    }

    /**
     * Сгенерировать ответ клиенту
     * @return 
     */
    @Override
    public AbstractMessage generateAnswer() {
        Map<String, Object> data = new HashMap<>();
        MessageType type;
        
        if(doAction()){

            tokensCache.generateToken(user);
            data.put(AbstractIncomingMessage.FIELD_TOKEN, user.getToken());
            type = MessageType.CUSTOMER_SUCCESS;
        } else {
            data.put(AbstractMessage.FIELD_DATA_ERROR_TEXT, "Не удалось войти в систему");
            type = MessageType.CUSTOMER_ERROR;
        }
        
        return new AbstractMessage(type, this.getSequenceId(), data);
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.Messages;

import java.io.StringReader;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParsingException;

/**
 *
 * @author Kostya
 */
public abstract class AbstractIncomingMessage extends AbstractMessage{
    public final static String FIELD_TOKEN = "api_token";
    
    private final String token;
    
    AbstractIncomingMessage(String json) throws JsonException, JsonParsingException{
        super(json);
        
        try(JsonReader jr = Json.createReader(new StringReader(json))){
            JsonObject jo = jr.readObject();
            token = jo.getString(FIELD_TOKEN);
        }
    }
   
    
    AbstractIncomingMessage(MessageType messageType, String sequenceId, String token, Map<String, Object> data){
        super(messageType, sequenceId, data);
        this.token = token;
    }
    
    public String getToken(){
        return this.token;
    }
    
    
    abstract boolean doAction();
    
    public abstract AbstractMessage generateAnswer();
    
}

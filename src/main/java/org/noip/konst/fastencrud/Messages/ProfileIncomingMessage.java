/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.Messages;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.json.JsonObject;
import org.noip.konst.fastencrud.Account.Account;
import org.noip.konst.fastencrud.Account.AccountController;
import org.noip.konst.fastencrud.Tokens.TokensCache;

/**
 *
 * @author Kostya
 */
public class ProfileIncomingMessage extends AbstractIncomingMessage{

    private Account user;
    private final AccountController accountController;
    private final TokensCache tokensCache;
    
    ProfileIncomingMessage(String jsonString, AccountController ac, TokensCache tc){
        super(jsonString);
        this.accountController = ac;
        this.tokensCache = tc;
    }
    
    @Override
    boolean doAction() {
        user = tokensCache.getUser(this.getToken());
        
        return user != null;
    }

    @Override
    public AbstractMessage generateAnswer() {
        Map<String, Object> data = new HashMap<>();
        
        if(!tokensCache.checkToken(getToken())){
            data.put(AbstractMessage.FIELD_DATA_ERROR_TEXT, "Токен не активен.");
            return new AbstractMessage(MessageType.CUSTOMER_ERROR, this.getSequenceId(), data);
        }
        
        if(doAction()){
            
            Set<String> keySet = user.getProfile().toJson().keySet();
            JsonObject jo = user.getProfile().toJson();
            
            keySet.forEach(k -> data.put(k, jo.getString(k)));
            
            return new AbstractMessage(MessageType.CUSTOMER_SUCCESS, this.getSequenceId(), data);
        
        } else {

            data.put(AbstractMessage.FIELD_DATA_ERROR_TEXT, "Не удалось получить профиль пользователя");
            return new AbstractMessage(MessageType.CUSTOMER_ERROR, this.getSequenceId(), data);
        
        }
    }
}

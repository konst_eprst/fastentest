/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.Account;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Kostya
 */

@ApplicationScoped
public class AccountController implements Serializable{
    
    @PersistenceContext(unitName = "FastenPU")
    private EntityManager em;
    
    @PostConstruct
    private void init(){
        System.out.println("UserCOntroller инициализирован");
    }
    
    /**
     * Аутентификация
     * @param login
     * @param password
     * @return
     * @throws NoResultException 
     */
    public Account authenticate(String login, String password) throws NoResultException{
        TypedQuery<Account> q = em.createQuery("SELECT a FROM Accounts a WHERE a.password = :pass AND a.login = :login", Account.class);
        return q.setParameter("pass", password).setParameter("login", login).getSingleResult();
    }
}

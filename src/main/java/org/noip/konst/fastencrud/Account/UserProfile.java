/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.Account;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 *
 * @author Kostya
 */
@Entity
@Table(name = "UserProfiles")
public class UserProfile implements Serializable{
    private static final long serialVersionUID = 1L;
    
    public final static String FIELD_LASTNAME = "lastName";
    public final static String FIELD_FIRSTNAME = "firstName";
    public final static String FIELD_PHOTO = "photo";

    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "profile", cascade = CascadeType.ALL)
    private Collection<Account> accounts;
    
    @Column(length = 45, name = FIELD_FIRSTNAME)
    private String firstName;
    
    @Column(length = 45, name = FIELD_LASTNAME)
    private String lastName;
    
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(length = 65535, name = FIELD_PHOTO)
    private byte[] photo;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the accounts
     */
    public Collection<Account> getAccounts() {
        return accounts;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the photo
     */
    public byte[] getPhoto() {
        return photo;
    }

    /**
     * @param photo the photo to set
     */
    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final UserProfile other = (UserProfile) obj;
        
        return Objects.equals(this.id, other.id);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
    public JsonObject toJson(){
        JsonObjectBuilder builder = Json.createObjectBuilder();
        
        builder.add(FIELD_FIRSTNAME, this.firstName);
        builder.add(FIELD_LASTNAME, this.lastName);
        if(this.photo != null){
            builder.add(FIELD_PHOTO, new String(this.photo));
        } else {
            builder.add(FIELD_PHOTO, "");
        }
        
        
        return builder.build();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.Account;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;




/**
 *
 * @author Kostya
 */
@Entity(name = "Accounts")
@Table(name = "Accounts")
public class Account implements Serializable{
    private static final long serialVersionUID = 2L;
    
    public static final String FIELD_LOGIN = "login";
    public static final String FIELD_PASSWORD = "password";
    
    @Transient
    private String token;

    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer accountId;

    
    @Column(name = "login")
    private String login;
    
    @Column(name = "password")
    private String password;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="profileId", referencedColumnName = "id")
    private UserProfile profile;
    
    
    
    public Integer getAccountId(){
        return this.accountId;
    }
    
    public String getToken(){
        return this.token;
    }
    
    public void setToken(String token){
        this.token = token;
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Account other = (Account) obj;
        if (!Objects.equals(this.accountId, other.accountId)) {
            return false;
        }
        return true;
    }

    public UserProfile getProfile() {
        return profile;
    }

    public void setProfile(UserProfile profile) {
        this.profile = profile;
    }
    

}

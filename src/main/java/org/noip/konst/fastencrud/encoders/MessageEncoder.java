/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud.encoders;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import org.noip.konst.fastencrud.Messages.AbstractMessage;

/**
 *
 * @author Kostya
 */
public class MessageEncoder implements Encoder.Text<AbstractMessage>{

    @Override
    public String encode(AbstractMessage arg0) throws EncodeException {
        return arg0.toJson().toString();
    }

    @Override
    public void init(EndpointConfig config) {
        System.out.println("Кодер инициализирован");
    }

    @Override
    public void destroy() {
        System.out.println("Кодер уничтожен");
    }
    
}

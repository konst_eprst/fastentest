/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.noip.konst.fastencrud;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.noip.konst.fastencrud.Messages.*;
import org.noip.konst.fastencrud.decoders.MessageDecoder;
import org.noip.konst.fastencrud.encoders.MessageEncoder;

/**
 *
 * @author Kostya
 */
@ApplicationScoped
@ServerEndpoint(value = "/endpoint"
        , decoders = MessageDecoder.class
        , encoders = MessageEncoder.class)
public class Main {
    
    @Inject
    private MessageHandler messageHandler;  
    
    @OnMessage
    public void onMessage(AbstractIncomingMessage message, Session session) {
        
        AbstractMessage answer = messageHandler.proccesIncomingMessage(message);
        try {
            session.getBasicRemote().sendObject(answer);
        } catch (IOException | EncodeException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @OnClose
    public void onClose(Session session){
        
    }
    
}
